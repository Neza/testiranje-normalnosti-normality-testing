# README #

### Čemu je repozitorij namenjen? ###

* Projektu Testiranje normalnosti pri predmetu Matematika z računalnikom v študijskem letu 2016/2017.
* Kratek opis projekta: implementacija Anderson-Darlingovega testa, ki testira, ali opaženi podatki izhajajo iz normalne (Gaussove) porazdelitve.

### Repozitorij vsebuje ###

* podatke
    * tabela `DM.csv`,
    * tabela `Prsni_kosi.csv`, ki smo jih prepisali iz tabele `PK.csv` s programom `prepisPodatkovPrsniKosi.py`,
    * tabela `Smucarke.csv`,
    * tabela `Visine_vojakov.csv`,
* tabelo `AD.csv` s podatki, ki jih vrne Anderson-Darlingov test,
* datoteko `AinMC.R`, ki računa testno statistiko A za opažene podatke in izvede Monte Carlo simulacijo za izračun ali oceno p-vrednosti testa,
* datoteko `transformacija.R`, ki transformira opažene podatke, da so bližje normalni porazdelitvi,
* Shiny, Shiny: datoteki `server.R` in `ui.R`, s katerima lahko preko uporabniškega vmesnika enostavno dostopamo do dobljenih grafičnih in računski rezultatov,
* mapo `jpgFiles`, kjer so zbrani vsi histogrami, qq-grafikoni in prikaz časovne vrste za podatke iz `DM.csv`,
* mapo `ShortProjectPresentation`, ki vsebuje kratko predstavitev projekta s sredine semestra,
* mapo `FinalProjectPresentation`, ki vsebuje končno predstavitev projekta ob koncu semestra.

### Več informacij ###

* neza.dimec@student.fmf.uni-lj.si